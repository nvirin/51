<?php

/*
 * This file is part of the Cocorico package.
 *
 * (c) Cocolabs SAS <contact@cocolabs.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

//namespace Cocorico\CoreBundle\Controller;
namespace Cocorico\CoreBundle\Controller\Frontend;

use Cocorico\CoreBundle\Entity\Booking;
use Cocorico\CoreBundle\Entity\Listing;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Booking controller.
 *
 * @Route("/booking")
 */
class BookingPriceController extends Controller
{

    private function routeToControllerName($routename) {
        $routes = $this->get('router')->getRouteCollection();
        return $routes->get('cocorico_booking_new')->getDefaults('_controller');
    }
    /**
     * Creates a new Booking price form.
     *
     * @param  Listing $listing
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function bookingPriceFormAction(Listing $listing)
    {
        $bookingPriceHandler = $this->get('cocorico.form.handler.booking_price');
        $booking = $bookingPriceHandler->init($this->getUser(), $listing);

        $form = $this->createBookingPriceForm($booking);

        return $this->render(
            '@CocoricoCore/Frontend/Booking/form_booking_price.html.twig',
            array(
                'form' => $form->createView(),
                'booking' => $booking
            )
        );
    }

    /**
     * Creates a form for Booking Price.
     *
     * @param Booking $booking The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createBookingPriceForm(Booking $booking)
    {
        $form = $this->get('form.factory')->createNamed(
            '',
            'booking_price',
            $booking,
            array(
                'method' => 'POST',
                'action' => $this->generateUrl(
                    'cocorico_booking_price',
                    array(
                        'listing_id' => $booking->getListing()->getId()
                    )
                )
            )
        );

        return $form;
    }


    /**
     * Get Booking Price
     *
     * @Route("/{listing_id}/price", name="cocorico_booking_price", requirements={"listing_id" = "\d+"})
     * @Security("is_granted('booking', listing)")
     *
     *
     * @ParamConverter("listing", class="CocoricoCoreBundle:Listing", options={"id" = "listing_id"})
     *
     * @Method({"POST"})
     *
     * @param Request  $request
     * @param  Listing $listing
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function getBookingPriceAction(Request $request, Listing $listing)
    {
        $bookingPriceHandler = $this->get('cocorico.form.handler.booking_price');
        $booking = $bookingPriceHandler->init($this->getUser(), $listing);
        $booking->setnombrepersonne($this->get('session')->set('nombrepersonne', $booking->getnombrepersonne()));

        $form = $this->createBookingPriceForm($booking);
        $form->handleRequest($request);
        $booking->setnombrepersonne($booking->getnombrepersonne());
        $this->get('session')->set('nombrepersonne', $booking->getnombrepersonne());
        //var_dump($this->get('session')->get('nombrepersonne'));
//        if(null !==$this->get('session')->get('nombrepersonne')){
//            return
//                $this->render(
//                    '@CocoricoCore/Frontend/Booking/form_booking_price.html.twig',
//                    array(
//                        'form' => $form->createView(),
//                        'booking' => $booking
//                    )
//                );
//
//        }

        //Return form if Ajax request
        if ($request->isXmlHttpRequest()) {
            //var_dump($request);
            return
                $this->render(
                    '@CocoricoCore/Frontend/Booking/form_booking_price.html.twig',
                    array(
                        'form' => $form->createView(),
                        'booking' => $booking
                    )
                );

        } else {//Redirect to new Booking page if no ajax request
//            return $this->redirect(
//                $this->generateUrl(
//                    'cocorico_booking_new',
//                    array(
//                        'listing_id' => $listing->getId(),
//                        'start' => $booking->getStart()->format('Y-m-d'),
//                        'end' => $booking->getEnd()->format('Y-m-d'),
//                        'start_time' => $booking->getStartTime() ? $booking->getStartTime()->format('H:i') : "00:00",
//                        'end_time' => $booking->getEndTime() ? $booking->getEndTime()->format('H:i') : "00:00",
//                        'nombrepersonne' => $booking->getnombrepersonne(),
//                    )
//                )
//            );

            // redirect to a route with parameters
            return $this->redirectToRoute('cocorico_booking_new',

                array(
                    'listing_id' => $listing->getId(),
                    'n' => $booking->getnombrepersonne(),
                    'start' => $booking->getStart()->format('Y-m-d'),
                    'end' => $booking->getEnd()->format('Y-m-d'),
                    'start_time' => $booking->getStartTime() ? $booking->getStartTime()->format('H:i') : "00:00",
                    'end_time' => $booking->getEndTime() ? $booking->getEndTime()->format('H:i') : "00:00",

                )
            );

//            return $this->forward('CocoricoCoreBundle\Frontend:Booking', array(
//                'listing_id' => $listing->getId(),
//                        'start' => $booking->getStart()->format('Y-m-d'),
//                        'end' => $booking->getEnd()->format('Y-m-d'),
//                        'start_time' => $booking->getStartTime() ? $booking->getStartTime()->format('H:i') : "00:00",
//                        'end_time' => $booking->getEndTime() ? $booking->getEndTime()->format('H:i') : "00:00",
//                        'nombrepersonne' => $booking->getnombrepersonne(),
//            ));





        }
    }
}
