<?php

/*
 * This file is part of the Cocorico package.
 *
 * (c) Cocolabs SAS <contact@cocolabs.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cocorico\CoreBundle\Event;

use Cocorico\CoreBundle\Entity\BookingBankWire;
use Cocorico\CoreBundle\Model\Manager\BookingBankWireManager;
use Monolog\Logger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Psr\Log\LoggerInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Mwork\MstripeBundle\Stripe\StripeClient;



use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;


class BookingBankWireSubscriber implements EventSubscriberInterface
{
    protected $bookingBankWireManager;
    protected $logger;

    /**
     * @param BookingBankWireManager $bookingBankWireManager
     * @param Logger                 $logger
     */
    public function __construct(BookingBankWireManager $bookingBankWireManager, Logger $logger)
    {
        $this->bookingBankWireManager = $bookingBankWireManager;
        $this->logger = $logger;
    }

    /**
     * Check the booking bank wire status.
     * By default checking result is true because there is no payment system and all payment related things are
     * simulated.
     *
     * @param BookingBankWireEvent $event
     *
     * @return bool
     * @throws \Exception
     */
    public function onBookingBankWireCheck(BookingBankWireEvent $event)
    {
        $bookingBankWire = $event->getBookingBankWire();
        $date=new \DateTime();
        $date=$date->format('Y-m-d H:i:s');

        try{



            $booking=$bookingBankWire->getBooking();
            $amountTotalReceivebyOffer = $booking->getAmountToPayToOffererDecimal();
            $amountTotal=$booking->getAmountTotal();

            $amountTotalReceivebyOfferStripe=$amountTotalReceivebyOffer*100;
            $amountTotalFee=$amountTotal-$amountTotalReceivebyOfferStripe;
            $chargeCurrency='eur';
            $customerId=$booking->getcustomerid();
//
            $applicationFee = $amountTotalFee;// a editer
            $chargeDescription = 'aventour';// a editer
            $chargeMetadata = [];// a editer
//
//        $stripemanager=$this->container->get('mwork_mstripe.stripe.client');
            $stripemanager=new StripeClient('sk_live_OUvlaUB3glEDn380VuFRNNpa');

            $offerer = $booking->getListing()->getUser();
            $ConnectstripeAccountId =$offerer->getconnectStripeId();
            //throw new \Exception('coucou');
            $token=$stripemanager->tokenShareCustomerToConnect($customerId,  $ConnectstripeAccountId)->id;

            $chargecustomeridm=$stripemanager->mcreateCharge($amountTotal, $chargeCurrency, $token, $ConnectstripeAccountId , $applicationFee, $chargeDescription, $chargeMetadata );


        }  catch (\Exception $e) {
            //In case of error while payment for example
            //var_dump($chargecustomeridm
            //print_r($e);
            //echo '$e';

//            getMessage();                 // Exception message
//            public function getCode();                    // User-defined Exception code
//    public function getFile();                    // Source filename
//    public function getLine();                    // Source line
//    public function getTrace();                   // An array of the backtrace()
//    public function getTraceAsString();
            // echo 'Message: '.$e->getMessage();echo '<br><br>';
//            echo 'Code: '.$e->getCode();echo '<br><br>';
//            echo 'File: '.$e->getFile();echo '<br><br>';
//            echo 'Line: '.$e->getLine();echo '<br><br>';
//            echo 'Trace: '.$e->getTrace();echo '<br><br>';
//            echo 'traceasstring:<br> '.$e->getTraceAsString();
//            echo '<br><br>';



            $subject='Erreur payment to offerer '.$date.' '.$offerer->getId().' '.$offerer->getEmail().' BookBankWireId'.$bookingBankWire->getId();
            $message='Id: '.$offerer->getId().'
                     BookbankwireID: '.$bookingBankWire->getId().'
                  Email: '.$offerer->getEmail().'
                  Date: '.$date.'


                  Amount total receive by offerer
                  '. $amountTotalReceivebyOfferStripe.'

                  Amount total
                  '.$amountTotal.'

                  Amount totla fee
                  '.$amountTotalFee.'

                 Customer ID
                 '.$customerId.'

                  Connect stripe account id
                  '.$ConnectstripeAccountId.'

                  Token
                  '.$token.'

                   Retour exeption e message
                  '.$e->getMessage().'
                  
                   Retour exeption e code
                  '.$e->getCode().'
                  
                   Retour exeption e file
                  '.$e->getFile().'
                  
                   Retour exeption e line
                  '.$e->getLine().'
                  
                   Retour exeption e trace
                  '.$e->getTrace().'
                  
                   Retour exeption e trace as string
                  '.$e->getTraceAsString().'
                  
                  
                  
                  


                  '




            ;
            // echo $message;

            $this->bookingBankWireManager->getMailer()->sendMessageToAdmin($subject, $message);
            $bookingBankWire->setStatus(BookingBankWire::STATUS_FAILED);
            // $bookingBankWire->setPayedAt(new \DateTime());
            $bookingBankWire = $this->bookingBankWireManager->save($bookingBankWire);





            return false;
        }



        $subject='BookingBankWireSubriscriber payed '.$date.' '.$offerer->getId().' '.$offerer->getEmail().' BookBankWireId'.$bookingBankWire->getId();
        $message='Id: '.$offerer->getId().'
                     BookbankwireID: '.$bookingBankWire->getId().'
                  Email: '.$offerer->getEmail().'
                  Date: '.$date.'


                  Amount total receive by offerer
                  '. $amountTotalReceivebyOfferStripe.'

                  Amount total
                  '.$amountTotal.'

                  Amount totla fee
                  '.$amountTotalFee.'

                 Customer ID
                 '.$customerId.'

                  Connect stripe account id
                  '.$ConnectstripeAccountId.'

                  Token
                  '.$token.'

     
                  
                  
                  


                  '




        ;


        $this->getLogger()->debug(
            'BookingBankWireManager Transaction Payed:' .
            '|-BookingBankWire Id:' . $bookingBankWire->getId()
        );

        $bookingBankWire->setStatus(BookingBankWire::STATUS_PAYED);
        $bookingBankWire->setPayedAt(new \DateTime());
        $bookingBankWire = $this->bookingBankWireManager->save($bookingBankWire);
        $this->bookingBankWireManager->getMailer()->sendWireTransferMessageToOfferer($bookingBankWire->getBooking());
        $this->bookingBankWireManager->getMailer()->sendMessageToAdmin($subject, $message);

        $event->setChecked(true);
        $event->setBookingBankWire($bookingBankWire);
        $event->stopPropagation();
        return true;
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            BookingBankWireEvents::BOOKING_BANK_WIRE_CHECK => array('onBookingBankWireCheck', 1),
        );
    }

}