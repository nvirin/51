<?php

namespace Mwork\MstripeBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ConnectStripe
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mwork\MstripeBundle\Entity\ConnectStripeRepository")
 */
class ConnectStripe 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

      /*
     * 
     *
     *  
     * 
     * 
     */
     //  @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     // * @ORM\OneToOne(targetEntity="Cocorico\UserBundle\Entity\User", inversedBy="connectStripe")
    //private $user;


    /**
     * @var string
     *
     * @ORM\Column(name="ConnectStripeId", type="string", length=255)
     * 
     * 
     */
    //@ORM\OneToOne(targetEntity="Cocorico\UserBundle\Entity\User", inversedBy="connectStripe")
    private $connectStripeId;

  

   


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set connectStripeId
     *
     * @param string $connectStripeId
     *
     * @return ConnectStripe
     */
    public function setConnectStripeId($connectStripeId)
    {
        $this->connectStripeId = $connectStripeId;

        return $this;
    }

    /**
     * Get connectStripeId
     *
     * @return string
     */
    public function getConnectStripeId()
    {
        return $this->connectStripeId;
    }

    /*
     * Set user
     *
     * @param integer $user
     *
     * @return ConnectStripe
     */
    // public function setuser($user)
    // {
    //     $this->user = $user;

    //     return $this;
    // }

    /*
     * Get user
     *
     * @return integer
    //  */
    // public function getuser()
    // { 
    //     return $this->user;
    // }
}

