<?php

namespace Mwork\MstripeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
//use Mwork\Mstripe\StripeBundle\Stripe;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Symfony\Component\Security\Core\Security;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
//use Mwork\MstripeBundle\Entity\ConnectStripe;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

//use Maknz\Slack\Attachment;
use Maknz\Slack\Client;




class DefaultController extends Controller
{
    /**
     * @Route("/he/{name}")
     * @Template()
     */
    public function indexAction($name)
    {


        $slack = $this->get('nexy_slack.client');
        $mailer = $this->get('cocorico.mailer.twig_swift');
        try{
            $mailer->sendMessageToAdmin('subject', 'message');
//            $slack = new Client('https://hooks.slack.com/services/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');
//
//            $message = $slack->createMessage();
//            $text='This is an amazing message!'.time().'
//
//More info about attachment on <https://api.slack.com/docs/formatting|Slack documentation>!';
//            $message
//                ->to('#stripe-aventour')
//                ->from('John Doe')
//                ->withIcon(':ghost:')
//                ->setText($text)
//            ;
//
//            $slack->sendMessage($message);




            //$client = new Maknz\Slack\Client('https://hooks.slack.com/...');

// Instantiate with defaults, so all messages created
//// will be sent from 'Cyril' and to the #accounting channel
//// by default. Any names like @regan or #channel will also be linked.
//            $settings = [
//                'username' => 'Cyril',
//                'channel' => '#stripe-aventour',
//                'link_names' => true
//            ];
//
//            $client = new Maknz\Slack\Client('https://hooks.slack.com/services/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', $settings);

        }catch (Exception $e) {
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }


        //$slack = new Client();




//        $message->attach(new Attachment([
//            'color'     => '#CCC',
//            'text'      => 'More info about attachment on <https://api.slack.com/docs/formatting|Slack documentation>!',
//            'mrkdwn_in' => ['text'],
//        ]));






        return array('name' => $name);


    }

    /**
     *
     * @Route("/becomeproform", name="becomeproform")
     * @Security("has_role('ROLE_USER')")
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function becomeproformAction()
    {

        $url =  $this->container->get('router')->generate('testm0');
        // $url='coucou';
       // var_dump($url);die;



        $response = $this->container->get('templating')->renderResponse('@MworkMstripe/Frontend/Page/becomeproform.html.twig', array(
            // 'form' => $form->createView(),
            'urlaction'=>$url,
        ));

        return $response;
    }



}  
