<?php

namespace Mwork\MstripeBundle\Security;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
 
class ConnectstripeVoter implements VoterInterface
{
    public function supportsAttribute($attribute)
    {
        return 1 === preg_match('/^ROLE_CONNECTSTRIPE_/', $attribute);
    }

    public function supportsClass($class) 
    {
        return true;
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        $vote = VoterInterface::ACCESS_ABSTAIN;

        foreach ($attributes as $attribute) {
            if (false === $this->supportsAttribute($attribute)) {
                continue;
            }
 
            $user = $token->getUser();
            $vote = VoterInterface::ACCESS_DENIED;
 
            // Check if the current user is the auhtor of the article// c'est ici que tout se passe

       //      $session = $this->container->get('security.token_storage');
       // $usersessionid=$session->getToken()->getUser()-> getId();

        
       //  $user = $this->container->get('cocorico_user.user_manager')->getRepository()->findOneById($usersessionid);
       //  var_dump($user->getId());
       //  var_dump($user->getconnectStripeId());
       // die; 
            if ($object->getAuthor()->getId() === $user->getId()) {
                $vote = VoterInterface::ACCESS_GRANTED;
            }
        }   

        return $vote;
    }
}